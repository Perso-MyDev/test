﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<Element>();
            list.Add(new Carre()
            {
                Nom = "carre1",
                Superficie = 6403
            });
            list.Add(new Triangle()
            {
                Nom = "triangle1",
                Superficie = 12
            });
            list.Add(new Cercle()
            {
                Nom = "cercle1",
                Superficie = 98
            });
            list.Print();
            Console.WriteLine("Hello, que veux tu ajouter ?");
            Console.WriteLine("_______________________________");

            var keepGoing = true;
            var pJson = @"c:\dev\collection.json";
            var service = new ArgsService(list, pJson, new List<IPricer>() { new Pricer(), new OtherPricer() });

            while (keepGoing)
            {
                Console.WriteLine("hint : syntaxe = nature nom superficie");
                Console.WriteLine(@"Après l'ajout, le json sera sauvegardé ici : " + pJson);
                var newT = Console.ReadLine();
                service.DoWork(newT);
                Console.WriteLine();
                Console.WriteLine("==============");
                Console.WriteLine("Tu veux encore ajouter un element ? [Y/N]");
                keepGoing = Console.ReadLine().ToLower().Trim() != "n";
            }
        }
    }

    public class ArgsService
    {
        public List<Element> List { get; set; }
        public List<IPricer> Pricers { get; set; }
        public string[] Args { get; set; }
        public string JsonPath { get; set; }
        public ArgsService(List<Element> list, string pJson, List<IPricer> pricers)
        {
            List = list;
            JsonPath = pJson;
            Pricers = pricers;
        }
        public void SaveJSon()
        {
            var path = JsonPath;
            if (Args.Length == 2 && Args[0].ToLower() != "save")
            {
                path = Args[1];
            }
            System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(List, Formatting.Indented));
        }
        private bool AddElement()
        {
            var elem = CreateElementByTypeName(Args[0].ToLower());
            if (Args.Length != 3 || elem == null)
            {
                return false;
            }
            if (elem != null)
            {
                elem.Nom = Args[1];
                elem.Superficie = int.Parse(Args[2]);
                List.Add(elem);

                List.Print();
            }
            return true;
        }
        public Element CreateElementByTypeName(string typeName)
        {
            switch (typeName)
            {
                case "carre":
                    return new Carre();
                case "triangle":
                    return new Triangle();
                case "cercle":
                    return new Cercle();
                default:
                    return null;

            }
        }
        public void Pricer()
        {
            NewLine();
            Console.WriteLine("Pricing...");
            var listPricers = new List<IPricer>() {
                new Pricer(),
                new OtherPricer()
            };
            foreach (var pricer in listPricers)
            {
                NewLine();
                Console.WriteLine("Pricer name : " + pricer.GetType().Name);
                Pricer(pricer);
            }
        }
        public void Pricer<T>(T pricer) where T : IPricer
        {
            foreach (var el in List)
            {
                el.Print(pricer.Price(el.Superficie));
            }
        }
        public void DoWork(string argsT)
        {
            NewLine();
            Console.WriteLine("Doing...");
            Args = argsT.Split(' ');
            if (Args.Length > 2)
            {
                AddElement();
                SaveJSon();
                Pricer();
            }
            else
                List.Print();
        }
        public void NewLine()
        {
            Console.WriteLine();
        }
    }

    #region "models"
    public abstract class Element
    {
        public string Nom { get; set; }
        public int Superficie { get; set; }
        public abstract string Type { get; }
        public override string ToString()
        {
            return Type + " - " + Nom + " - " + Superficie;
        }
    }
    public abstract class Element<T> : Element
    {
        public override string Type => typeof(T).Name;
    }
    public class Triangle : Element<Triangle> { }
    public class Cercle : Element<Cercle> { }
    public class Carre : Element<Carre> { }
    #endregion

    public static class Extensions
    {
        public static void Print(this IEnumerable<Element> list)
        {
            foreach (var el in list)
            {
                Console.WriteLine(el.ToString());
            }
        }
        public static void Print(this Element el, int price)
        {
            Console.WriteLine(el.ToString() + " - Estimation cout :" + price + "€");
        }
    }

    #region "pricer"
    public interface IPricer { int Price(int superficie); }
    public class Pricer : IPricer { public int Price(int superficie) { return superficie * 2; } }
    public class OtherPricer : IPricer { public int Price(int superficie) { return superficie * 4; } }
    public class PricerElement
    {
        public IPricer Pricer { get; set; }

        public PricerElement(IPricer pricer)
        {
            Pricer = pricer;
        }

        public int Calculate(int superficie)
        {
            return Pricer.Price(superficie);
        }
    }
    #endregion
}
